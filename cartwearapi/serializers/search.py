from drf_haystack.serializers import HaystackSerializer
from .product import ProductSerializer
from cartwear.apps.search.search_indexes import ProductIndex


class ProductSearchSerializer(HaystackSerializer):

    class Meta:
        # The `index_classes` attribute is a list of which search indexes
        # we want to include in the search.
        index_classes = [ProductIndex]
        pk = ProductSerializer()

        # The `fields` contains all the fields we want to include.
        # NOTE: Make sure you don't confuse these with model attributes. These
        # fields belong to the search index!
        fields = [
            "title","upc","pk"
        ]
