from rest_framework import serializers
from cartwearapi.utils import (
    CartwearModelSerializer,
    overridable,
    CartwearHyperlinkedModelSerializer,
    CartwearStrategySerializer
)
from cartwear.core.loading import get_model,get_class
from .product import ProductLinkSerializer

ProductClass = get_model('catalogue','ProductClass')
Product = get_model('catalogue','Product')


class ProductClassLinkSerializer(CartwearHyperlinkedModelSerializer):

    class Meta:
        model = ProductClass
        fields = overridable(
            'CARTWEARAPI_PRODUCTCLASS_FIELDS',
            default=('name','url'))


class ProductClassSerializer(CartwearModelSerializer):

    name = serializers.CharField()
    products = ProductLinkSerializer()

    class Meta:
        model = ProductClass
        fields = overridable(
            'CARTWEARAPI_PRODUCTCLASS_DETAILS_FIELDS',
            default=('name','pk','products'))
