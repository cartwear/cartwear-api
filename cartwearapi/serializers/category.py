from rest_framework import serializers
from cartwearapi.utils import (
    CartwearModelSerializer,
    overridable,
    CartwearHyperlinkedModelSerializer,
    CartwearStrategySerializer
)
from cartwear.core.loading import get_model,get_class
from .product import ProductLinkSerializer

Category = get_model('catalogue','Category')
Product = get_model('catalogue','Product')



class CoreCategorySerializer(CartwearModelSerializer):
    name = serializers.CharField()

    class Meta:
        model = Category
        fields = overridable(
            'CARTWEARAPI_CATEGORY_DETAILS_FIELDS',
            default=('name','pk','description','image','has_children','get_num_children','get_descendants','url'))

class CategoryLinkSerializer(CoreCategorySerializer):

    get_descendants = CoreCategorySerializer(many=True)
    class Meta:
        model = Category
        fields = overridable(
            'CARTWEARAPI_CATEGORY_FIELDS',
            default=('name','pk','description','image','has_children','get_num_children','get_descendants','url'))


class CategorySerializer(CoreCategorySerializer):

    name = serializers.CharField()
    get_descendants = CoreCategorySerializer(many=True)

    class Meta:
        model = Category
        fields = overridable(
            'CARTWEARAPI_CATEGORY_DETAILS_FIELDS',
            default=('name','pk','description','image','has_children','get_num_children','get_descendants'))
