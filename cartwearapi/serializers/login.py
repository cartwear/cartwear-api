from django.contrib.auth import get_user_model, authenticate
from rest_framework import serializers

from cartwearapi.utils import overridable


User = get_user_model()

def field_length(fieldname):
    field = next(field for field in User._meta.fields if field.name == fieldname)
    return field.max_length

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = overridable('CARTWEARAPI_USER_FIELDS', (
            'username', 'id', 'date_joined',))
