from rest_framework import serializers

from cartwearapi.utils import (
    CartwearModelSerializer,
    overridable,
    CartwearHyperlinkedModelSerializer,
    CartwearStrategySerializer
)
from cartwear.core.loading import get_model,get_class
from .checkout import PriceSerializer

Product = get_model('catalogue', 'Product')
ProductAttribute = get_model('catalogue', 'ProductAttribute')
ProductAttributeValue = get_model('catalogue', 'ProductAttributeValue')
ProductImage = get_model('catalogue', 'ProductImage')
Option = get_model('catalogue', 'Option')
Partner = get_model('partner', 'Partner')

Selector = get_class('partner.strategy', 'Selector')


class PartnerSerializer(CartwearModelSerializer):
    class Meta:
        model = Partner


class ProductImageSerializer(CartwearModelSerializer):
    class Meta:
        model = ProductImage


class OptionSerializer(CartwearHyperlinkedModelSerializer):

    class Meta:
        model = Option
        fields = overridable('CARTWEARAPI_OPTION_FIELDS', default=[
            'url', 'id', 'name', 'code', 'type'
        ])


class ProductLinkSerializer(CartwearHyperlinkedModelSerializer):
    images = ProductImageSerializer(many=True)
    price = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = overridable('CARTWEARAPI_PRODUCT_FIELDS', default=('url',
                                                           'id',
                                                           'title','images','price'))

    def get_price(self, obj):
        request = self.context.get('request')
        strategy = Selector().strategy(request=request, user=request.user)
        price = strategy.fetch_for_product(obj).price
        return PriceSerializer(price).data

class ProductAttributeValueSerializer(CartwearModelSerializer):
    attribute = serializers.CharField(read_only=True)
    value = serializers.SerializerMethodField( read_only = True)

    def get_value(self, obj):
        return obj.value

    class Meta:
        model = ProductAttributeValue
        fields = ('attribute', 'value',)


class ProductAttributeSerializer(CartwearModelSerializer):
    productattributevalue_set = ProductAttributeValueSerializer(many=True)

    class Meta:
        model = ProductAttribute
        fields = ('name', 'productattributevalue_set')


class ProductAvailabilitySerializer(CartwearStrategySerializer):
    is_available_to_buy = serializers.BooleanField(
        source="info.availability.is_available_to_buy")
    num_available = serializers.IntegerField(
        source="info.availability.num_available")
    message = serializers.CharField(source="info.availability.message")


class RecommmendedProductSerializer(CartwearModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='product-detail')
    class Meta:
        model = Product
        fields = overridable('CARTWEARAPI_RECOMMENDED_PRODUCT_FIELDS',
                                 default=('url',))


class ProductSerializer(CartwearModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='product-detail')
    stockrecords = serializers.HyperlinkedIdentityField(
        view_name='product-stockrecord-list')
    attributes = ProductAttributeValueSerializer(many=True,
                                                 required=False,
                                                 source="attribute_values")
    # categories = serializers.HyperlinkedRelatedField(read_only = True, view_name='categories')
    product_class = serializers.HyperlinkedRelatedField(read_only = True, view_name='productclass-detail')
    images = ProductImageSerializer(many=True)

    price = serializers.SerializerMethodField()
    # price = serializers.HyperlinkedIdentityField(view_name='product-price')
    availability = serializers.HyperlinkedIdentityField(
        view_name='product-availability')
    # options = OptionSerializer(many=True, required=False)
    recommended_products = RecommmendedProductSerializer(many=True,
                                                         required=False)

    class Meta:
        model = Product
        fields = overridable(
            'CARTWEARAPI_PRODUCTDETAIL_FIELDS',
            default=('url', 'id', 'title', 'description',
                     'date_created', 'date_updated', 'recommended_products',
                     'attributes', 'stockrecords', 'images', 'price',
                     'availability','product_class'))

    def get_price(self, obj):
        request = self.context.get('request')
        strategy = Selector().strategy(request=request, user=request.user)
        price = strategy.fetch_for_product(obj).price
        return PriceSerializer(price).data


class OptionValueSerializer(serializers.Serializer):
    option = serializers.HyperlinkedRelatedField(view_name='option-detail', queryset=Option.objects)
    value = serializers.CharField()


class AddProductSerializer(serializers.Serializer):
    """
    Serializes and validates an add to basket request.
    """
    quantity = serializers.IntegerField(required=True)
    product = serializers.PrimaryKeyRelatedField(
        queryset=Product.objects,
        allow_null=False)
    options = OptionValueSerializer(many=True, required=False)

    class Meta:
        model = Product
