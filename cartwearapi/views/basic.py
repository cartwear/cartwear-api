from decimal import Decimal
import functools
import itertools

from django.contrib import auth
from cartwear.core.loading import get_model, get_class
from rest_framework import generics
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from .mixin import PutIsPatchMixin
from .utils import NoPagination
from cartwearapi import serializers, permissions
from cartwearapi.basket.operations import assign_basket_strategy


Selector = get_class('partner.strategy', 'Selector')

__all__ = (
    'BasketList', 'BasketDetail',
    'LineAttributeList', 'LineAttributeDetail',
    'ProductList', 'ProductDetail',
    'ProductPrice', 'ProductAvailability',
    'CategoryList','CategoryDetail',
    'ProductClassList','ProductClassDetail',
    'StockRecordList', 'StockRecordDetail',
    'UserList', 'UserDetail',
    'OptionList', 'OptionDetail',
    'CountryList', 'CountryDetail',
    'PartnerList', 'PartnerDetail',
)

Basket = get_model('basket', 'Basket')
LineAttribute = get_model('basket', 'LineAttribute')
Product = get_model('catalogue', 'Product')
ProductClass = get_model('catalogue', 'ProductClass')
Category = get_model('catalogue','Category')
StockRecord = get_model('partner', 'StockRecord')
Option = get_model('catalogue', 'Option')
User = auth.get_user_model()
Country = get_model('address', 'Country')
Partner = get_model('partner', 'Partner')


# TODO: For all API's in this file, the permissions should be checked if they
# are sensible.
class CountryList(generics.ListAPIView):
    """
    Country List

    Get:
    List of all the country in the world so that the user can select.
    """
    serializer_class = serializers.CountrySerializer
    model = Country
    queryset = Country.objects.all()
    pagination_class = NoPagination


class CountryDetail(generics.RetrieveAPIView):
    serializer_class = serializers.CountrySerializer
    model = Country
    queryset = Country.objects.all()


class BasketList(generics.ListCreateAPIView):
    model = Basket
    queryset = Basket.objects.all()
    serializer_class = serializers.BasketSerializer
    permission_classes = (IsAdminUser,)

    def get_queryset(self):
        qs = super(BasketList, self).get_queryset()
        return itertools.imap(
            functools.partial(assign_basket_strategy, request=self.request),
            qs)

class BasketDetail(PutIsPatchMixin, generics.RetrieveUpdateDestroyAPIView):
    model = Basket
    queryset = Basket.objects.all()
    serializer_class = serializers.BasketSerializer
    permission_classes = (permissions.IsAdminUserOrRequestContainsBasket,)

    def get_object(self, queryset=None):
        basket = super(BasketDetail, self).get_object()
        return assign_basket_strategy(basket, self.request)

class LineAttributeList(generics.ListCreateAPIView):
    model = LineAttribute
    serializer_class = serializers.LineAttributeSerializer


class LineAttributeDetail(PutIsPatchMixin, generics.RetrieveAPIView):
    model = LineAttribute
    serializer_class = serializers.LineAttributeSerializer


class ProductList(generics.ListAPIView):
    """

    Get:
    List of all the products in the jumkey catalogue.

    Query Filtering:
    cat - Categories.
    pgt - Price Greater than.
    plt - Price Less than.
    """
    model = Product
    serializer_class = serializers.ProductLinkSerializer

    def get_queryset(self):
        """
        The query `cat` can be passed. For filtering out the categories.
        """
        queryset = Product.objects.all()
        cat = self.request.query_params.get('cat', None)
        plt = self.request.query_params.get('plt', None)
        pgt = self.request.query_params.get('pgt', None)
        if cat is not None and cat.isdigit():
            queryset = queryset.filter(categories=cat)
        try:
            plt = Decimal(plt)
        except:
            pass
        if plt is not None and  type(plt) is Decimal:
            queryset = queryset.filter(stockrecords__price_excl_tax__lte=plt)
        try:
            pgt = Decimal(pgt)
        except:
            pass
        if pgt is not None and type(pgt) is Decimal:
            queryset = queryset.filter(stockrecords__price_excl_tax__gte=pgt)

        return queryset


class ProductDetail(generics.RetrieveAPIView):
    model = Product
    queryset = Product.objects.all()
    serializer_class = serializers.ProductSerializer


class ProductPrice(APIView):

    def get(self, request, pk=None, format=None):
        product = Product.objects.get(id=pk)
        strategy = Selector().strategy()
        price = strategy.fetch_for_product(product).price
        ser = serializers.PriceSerializer(price,
                                          context={'request': request})
        return Response(ser.data)


class ProductAvailability(generics.RetrieveAPIView):
    model = Product
    queryset = Product.objects.all()
    serializer_class = serializers.ProductAvailabilitySerializer


class StockRecordList(generics.ListAPIView):
    model = StockRecord
    queryset = StockRecord.objects.all()
    serializer_class = serializers.StockRecordSerializer

    def get(self, request, pk=None, *args, **kwargs):
        if pk is not None:
            self.queryset = self.get_queryset().filter(product__id=pk)

        return super(StockRecordList, self).get(request, *args, **kwargs)


class StockRecordDetail(generics.RetrieveAPIView):
    model = StockRecord
    queryset = StockRecord.objects.all()
    serializer_class = serializers.StockRecordSerializer


class ProductClassList(generics.ListAPIView):
    model = ProductClass
    queryset = ProductClass.objects.all()
    serializer_class = serializers.ProductClassLinkSerializer


class ProductClassDetail(generics.RetrieveAPIView):
    model = ProductClass
    queryset = ProductClass.objects.all()
    serializer_class = serializers.ProductClassSerializer


class CategoryList(generics.ListAPIView):
    model = Category
    queryset = Category.objects.filter(depth=1)
    serializer_class = serializers.CategoryLinkSerializer
    pagination_class = NoPagination


class CategoryDetail(generics.RetrieveAPIView):
    model = Category
    queryset = Category.objects.all()
    serializer_class = serializers.CategorySerializer


class UserList(generics.ListAPIView):
    model = User
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = (IsAdminUser,)


class UserDetail(generics.RetrieveAPIView):
    model = User
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = (IsAdminUser,)


class OptionList(generics.ListAPIView):
    model = Option
    queryset = Option.objects.all()
    serializer_class = serializers.OptionSerializer


class OptionDetail(generics.RetrieveAPIView):
    model = Option
    queryset = Option.objects.all()
    serializer_class = serializers.OptionSerializer


class PartnerList(generics.ListAPIView):
    model = Partner
    queryset = Partner.objects.all()
    serializer_class = serializers.PartnerSerializer


class PartnerDetail(generics.RetrieveAPIView):
    model = Partner
    queryset = Partner.objects.all()
    serializer_class = serializers.PartnerSerializer
