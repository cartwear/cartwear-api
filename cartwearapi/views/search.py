from drf_haystack.viewsets import HaystackViewSet
from cartwearapi import serializers
from cartwearapi.serializers import search
from cartwear.core.loading import get_model
Product = get_model('catalogue', 'Product')


class ProductSearchView(HaystackViewSet):

    # `index_models` is an optional list of which models you would like to include
    # in the search result. You might have several models indexed, and this provides
    # a way to filter out those of no interest for this particular view.
    # (Translates to `SearchQuerySet().models(*index_models)` behind the scenes.
    index_models = [Product]

    serializer_class = search.ProductSearchSerializer
