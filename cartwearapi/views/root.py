import collections

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse


__all__ = ('api_root','auth_root')


def PUBLIC_APIS(r, f):
    return [
        ('auth', reverse('api-auth', request=r, format=f)),
        ('basket', reverse('api-basket', request=r, format=f)),
        ('basket-add-product', reverse('api-basket-add-product', request=r,
                                       format=f)),
        ('basket-add-voucher', reverse('api-basket-add-voucher', request=r,
                                       format=f)),
        ('basket-shipping-methods', reverse('api-basket-shipping-methods', request=r,
                                       format=f)),
        ('checkout', reverse('api-checkout', request=r, format=f)),
        ('orders', reverse('order-list', request=r, format=f)),
        ('products', reverse('product-list', request=r, format=f)),
        ('product-class', reverse('productclass-list', request=r, format=f)),
        ('category-list', reverse('category-list', request=r, format=f)),
        ('countries', reverse('country-list', request=r, format=f)),
    ]

def AUTH_APIS(r, f):
    return [
        ('login', reverse('login', request=r, format=f)),
        ('logout', reverse('logout', request=r, format=f)),
        ('me', reverse('user', request=r, format=f)),
        ('register', reverse('register', request=r, format=f)),
        ('activate', reverse('activate', request=r, format=f)),
        ('set_username', reverse('set_username', request=r, format=f)),
        ('set_password', reverse('set_password', request=r, format=f)),
        ('password_reset', reverse('password_reset', request=r, format=f)),
        ('password_reset_confirm', reverse('password_reset_confirm', request=r, format=f)),
    ]


def PROTECTED_APIS(r, f):
    return [
        ('lines', reverse('line-list', request=r, format=f)),
        ('lineattributes', reverse('lineattribute-list', request=r, format=f)),
        ('options', reverse('option-list', request=r, format=f)),
        ('stockrecords', reverse('stockrecord-list', request=r, format=f)),
        ('users', reverse('user-list', request=r, format=f)),
        ('partners', reverse('partner-list', request=r, format=f)),
    ]


@api_view(('GET',))
def api_root(request, format=None):
    """
    GET:
    Display all available urls.

    Since some urls have specific permissions, you might not be able to access
    them all.
    """
    apis = PUBLIC_APIS(request, format)
    if request.user.is_staff:
        apis += PROTECTED_APIS(request, format)

    return Response(collections.OrderedDict(apis))

@api_view(('GET',))
def auth_root(request, format=None):
    """
    GET:
    Display all the available urls for authetication.

    All the authentication login, logout, regisration, activating the account
    and changing the password.
    """
    apis = AUTH_APIS(request, format)
    return Response(collections.OrderedDict(apis))
