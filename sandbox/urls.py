from django.conf.urls import patterns, include, url
from django.contrib import admin
from cartwear.app import application
from cartwearapi.app import application as api

urlpatterns = patterns('',
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(api.urls)),
    url(r'', include(application.urls)),
)
